package test;

import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.LineUnavailableException;

import sound.SoundPlayer;
import sound.SoundResource;
import sound.SoundSystem;
import sound.decode.GenericDecoder;

public class Test {
	public static void main(String[] args) throws IOException, LineUnavailableException, InterruptedException {
		/*URL resource = Test.class.getResource("/samples/drums.mp3");
		Speaker[] speakers = SoundSystem.s_getAllSpeakers();
		for (Speaker s : speakers) System.out.println("Speaker: " + s.getName());
		
		SoundResource drums = new SoundResource(resource, new GenericDecoder());
		SoundResource music = new SoundResource(Test.class.getResource("/samples/music_sample.mp3"), new GenericDecoder());
		SoundSystem.s_getPlayer(speakers[1], drums).start();
		SoundSystem.s_getPlayer(speakers[1], music, 1f).start();*/
		
		SoundResource music = new SoundResource(Test.class.getResource("/samples/music_sample.mp3"), new GenericDecoder());
		AudioInputStream stream = music.openStream();
		SoundPlayer player = new SoundPlayer(stream.getFormat());
		player.open(SoundSystem.s_getDefaultSpeaker());
		player.start();
		player.play(stream);
		Thread.sleep(5000);
		player.pause(stream);
	}
}
