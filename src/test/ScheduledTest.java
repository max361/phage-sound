package test;

import java.io.IOException;

import sound.SoundResource;
import sound.SoundSystem;

public class ScheduledTest {
	public static void main(String[] args) throws IOException {
		SoundResource laserGun = new SoundResource(ScheduledTest.class.getResource("/samples/Laser_Gun.wav"));
		SoundResource laserMachinegun = new SoundResource(ScheduledTest.class.getResource("/samples/Laser_Machine_Gun.wav"));
		SoundResource shotgun = new SoundResource(ScheduledTest.class.getResource("/samples/shotgun.wav"));
		SoundResource machinegun = new SoundResource(ScheduledTest.class.getResource("/samples/machinegun.wav"));
		SoundResource[] sounds = {laserGun, laserMachinegun, shotgun, machinegun};
		while(true) {
			try {
				Thread.sleep((long) (4000 * Math.random()));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			SoundSystem.s_getPlayer(sounds[(int) ((sounds.length - 1) * Math.random())]).start();
		}
	}
}
