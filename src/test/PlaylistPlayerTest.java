package test;

import java.io.IOException;

import javax.sound.sampled.AudioInputStream;

import sound.PlaylistPlayer;
import sound.PlaylistPlayer.PlaylistPlayerListener;
import sound.SoundResource;
import sound.SoundSystem;

public class PlaylistPlayerTest {
	public static void main(String[] args) throws IOException, InterruptedException {
		final SoundResource music = new SoundResource(Test.class.getResource("/samples/music_sample.mp3"));
		
		AudioInputStream input = music.openStream();

		PlaylistPlayer player = new PlaylistPlayer(input.getFormat());
		player.open(SoundSystem.s_getDefaultSpeaker());
		player.add(input);
		player.addListener(new PlaylistPlayerListener() {
			@Override
			public void streamEnded(PlaylistPlayer player, AudioInputStream done) {
				System.out.println("Restarting");
				try {
					player.add(music.openStream());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void startedPlaying(PlaylistPlayer player, AudioInputStream n) {
				
			}
			
		});

		player.start();		

		//Will pause after five seconds

		Thread.sleep(5000);
		player.pause();
		Thread.sleep(2000);
		player.play();
	}
}
